var keystone = require('keystone');

/**
 * PassionCategory Model
 * ==================
 */

var PassionCategory = new keystone.List('PassionCategory', {
	autokey: { from: 'name', path: 'key', unique: true },
});

PassionCategory.add({
	name: { type: String, required: true },
});

PassionCategory.relationship({ ref: 'Post', path: 'posts', refPath: 'categories' });

PassionCategory.register();
