var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * VideoGallery Model
 * =============
 */

var Passions = new keystone.List('passions', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

Passions.add({
	// Each Entry has a (TODO - ask about this later)
	title: { type: String, required: true }, 
	url: { type: String },
	heroImage: { type: Types.CloudinaryImage },
	publishedDate: { type: Date, default: Date.now },
});

Passions.register();
