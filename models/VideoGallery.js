var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * VideoGallery Model
 * =============
 */

var VideoGallery = new keystone.List('videogalleries', {
	autokey: { from: 'name', path: 'key', unique: true },
});

VideoGallery.add({
	// Each Entry has a (TODO - ask about this later)
	name: { type: String, required: true },
	publishedDate: { type: Date, default: Date.now },
	heroImage: { type: Types.CloudinaryImage },
	images: { type: Types.CloudinaryImages },
	link: { type: String },  // TODO - make this required
});

VideoGallery.register();
