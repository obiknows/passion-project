var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'videos';

	// Load the galleries by sortOrder
	// loading the view goes  ( VideoGallery | 'videogalleries' <> )
	view.query('VideoGallery', keystone.list('videogalleries').model.find().sort('sortOrder'));

	// Render the view
	view.render('videos');

};
