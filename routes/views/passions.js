var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'passions';

	// Load the galleries by sortOrder
	// loading the view goes  ( VideoGallery | 'videogalleries' <> )
	view.query('Passions', keystone.list('passions').model.find().sort('sortOrder'));

	// Render the view
	view.render('passions');

};
